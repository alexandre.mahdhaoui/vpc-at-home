# vpc-at-home

## Install prerequisites
```shell
{
  dnf update -y && dnf upgrade -y
  dnf install -y sysstat git jq cloud-utils genisoimage\
   qemu-kvm libvirt virt-install bridge-utils \
   libvirt-devel virt-top libguestfs-tools guestfs-tools
  sudo systemctl enable --now libvirtd
}
```
## Generate ssh keys
```shell
ssh-keygen -t ed25519
```

## Prepare environment
```shell
{
  mkdir -p /virt/images /virt/templates /virt/cloud-init
}
```

## Install libvirt-utils
```shell
{
  curl -sLo /virt/libvirt-utils.sh https://gitlab.com/alexandre.mahdhaoui/vpc-at-home/-/raw/main/assets/libvirt-utils.sh
  echo ". /virt/libvirt-utils.sh"| tee -a ~/.bashrc
  . ~/.bashrc
}
```

## Download fedora37 cloud image
```shell
{
  curl -Lo /virt/templates/fedora37.qcow2 \
    https://download.fedoraproject.org/pub/fedora/linux/releases/37/Cloud/x86_64/images/Fedora-Cloud-Base-37-1.7.x86_64.qcow2
}
```

## Setup vpncloud
```shell
{
  sudo curl -Lo vpncloud.rpm https://github.com/dswd/vpncloud/releases/download/v2.3.0/vpncloud_2.3.0-1.x86_64.rpm
  sudo dnf install -y ./vpncloud.rpm
  rm -f vpncloud.rpm
}
```

```shell
{
  export PASS="YOUR_PASSWORD"
  bash <(curl -sL https://gitlab.com/alexandre.mahdhaoui/vpc-at-home/-/raw/main/assets/vpncloud-config-install.sh)
}
```

## Create network interfaces
```shell
{
  nmcli connection add type bridge con-name br0 ifname br0 ipv4.method disabled ipv6.method disabled
  nmcli connection add type bridge-slave autoconnect yes con-name vpc0 ifname vpncloud0 master br0
  nmcli connection up br0
}
```

```shell
{
  nmcli c && nmcli d
}
```

```shell
{
cat <<EOF > /usr/share/libvirt/networks/vpc0.xml
<network>
 <name>vpc0-bridge</name>
 <forward mode="bridge" />
 <bridge name="br0" />
</network>
EOF
}
```

```shell
{
  virsh net-define /usr/share/libvirt/networks/vpc0.xml
  virsh net-start vpc0-bridge
  virsh net-autostart vpc0-bridge
}
```

## Create the Internet Gateway
Setup the bridge
```shell
{
cat <<EOF > /usr/share/libvirt/networks/virbr0.xml
<network>
  <name>virbr0</name>
  <bridge name="virbr0"/>
  <forward mode="nat"/>
  <ip address="172.17.0.1" netmask="255.255.255.0">
    <dhcp>
      <range start="172.17.0.2" end="172.17.0.254"/>
    </dhcp>
  </ip>
</network>
EOF
}
```

Enable the bridge
```shell
{
  virsh net-define /usr/share/libvirt/networks/virbr0.xml
  virsh net-start virbr0
  virsh net-autostart virbr0
}
```

Provision the gateway
```shell
vm.new_gateway vpc0-gw0 fedora37
```

Setup the gateway
```shell
{
  HOSTNAME="vpc0-gw0"
  CONN="eth0c"
  IFACE="eth0"
  IP_ADDR="10.0.0.1"
  GATEWAY="10.0.0.1"
  CIDR="/8"
  vm.exec "${HOSTNAME}" "{
    sudo nmcli connection add con-name ${CONN} ifname ${IFACE} type ethernet
    sudo nmcli connection modify ${CONN} ipv4.addresses ${IP_ADDR}${CIDR}
    sudo nmcli connection modify ${CONN} ipv4.method manual
    sudo nmcli connection modify ${CONN} ipv4.gateway ${GATEWAY}
    sudo nmcli connection up ${CONN}
  }"
}
```

Install iptables
```shell
{
  HOSTNAME="vpc0-gw0"
  vm.exec "${HOSTNAME}" 'sudo dnf install -y iptables-services'
}
```

Forward traffic
```shell
{
  HOSTNAME="vpc0-gw0"
  IFACE_PRIVATE="eth0"
  IFACE_INTERNET="eth1"
  vm.exec "${HOSTNAME}" "{
    sudo modprobe ip_tables ip_conntrack
    sudo sysctl -w net.ipv4.ip_forward=1
    sudo iptables -A FORWARD -i ${IFACE_PRIVATE} -j ACCEPT
    sudo iptables -t nat -A POSTROUTING -o ${IFACE_INTERNET} -j MASQUERADE
    sudo iptables-save | sudo tee /etc/sysconfig/iptables
  }"
}
```

Setup DHCP server
```shell
{
  HOSTNAME="vpc0-gw0"
  VPC="vpc0"
  SUBNET="10.0.0.0"
  NETMASK="255.0.0.0"
  IP_RANGE="${SUBNET} 10.0.254.254"
  BROADCAST="10.255.255.255"
  ROUTER_IP="10.0.0.1"
  vm.exec "${HOSTNAME}" "{
    dnf -y install dhcp-server
cat << EOF | sudo tee /etc/dhcp/dhcpd.conf
option domain-name-servers     8.8.8.8;
default-lease-time 600;
max-lease-time 7200;
authoritative;
subnet ${SUBNET} netmask ${NETMASK} {
    range dynamic-bootp ${IP_RANGE};
    option broadcast-address ${BROADCAST};
    option routers ${ROUTER_IP};
}    
EOF
    systemctl enable --now dhcpd
  }"
}
```

## Provision a VMs for testing connectivity
```shell
{
  HOSTNAME="test0"
  vm.new "${HOSTNAME}" fedora37
}
```

```shell
{
  HOSTNAME="test0"
  vm.exec "${HOSTNAME}" "{
    
  }"
}
```


## Build VPNCloud
```shell
{
  for HOSTNAME in test0 test1; do
    vm.exec "${HOSTNAME}" '{
      sudo curl -Lo vpncloud.rpm https://github.com/dswd/vpncloud/releases/download/v2.3.0/vpncloud_2.3.0-1.x86_64.rpm
      sudo dnf install -y ./vpncloud.rpm
      rm -f vpncloud.rpm
    }'
  done
}
```

