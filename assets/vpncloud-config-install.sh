NET_PREFIX="10.0"
SUBNET="0"
cat <<EOF > /etc/vpncloud/vpc0.net
listen: 3210                # The port number or ip:port on which to listen for data.
peers:                      # Address of a peer to connect to.
  - 172.16.0.179:3210
  - 172.16.0.75:3210
crypto:                     # Crypto settings
  password: test
ip: ${NET_PREFIX}.${SUBNET}.1/8          # <-- CHANGE # An IP address to set on the device, e.g. 10.0.0.1
auto-claim: false            # Whether to automatically claim the configured IP on tun devices
device:                     # Device settings
  name: "vpc0%d"        # Name of the virtual device. Any "%d" will be filled with a free number.
  type: tap                 # Set the type of network. There are two options: **tap** devices process
  path: "/dev/net/tun"      # Path of the tun device
  fix-rp-filter: false       # Whether to fix detected rp-filter problems
ifup: >-
  sysctl -w net.ipv4.ip_forward=1 &&
    ifconfig \$IFNAME ${NET_PREFIX}.${SUBNET}.1/8 mtu 1400 &&
    iptables -t nat -A POSTROUTING -o $(ip route show default | awk '/default/ {print $5}') -j MASQUERADE &&
    ip route add ${NET_PREFIX}.${SUBNET}.0/8 dev \$IFNAME
ifdown: "ifconfig \$IFNAME down"
mode: normal                # Mode to run in, "normal", "hub", "switch", or "router" (see manpage)
port-forwarding: false       # Try to map a port on the router
switch-timeout: 300         # Switch timeout in seconds (switch mode only)
peer-timeout: 300           # Peer timeout in seconds
keepalive: ~                # Keepalive interval in seconds
beacon:                     # Beacon settings
  store: ~                  # File or command (prefix: "|") to use for storing beacons
  load: ~                   # File or command (prefix: "|") to use for loading beacons
  interval: 3600            # How often to load and store beacons (in seconds)
  password: ~               # Password to encrypt beacon data with
statsd:                     # Statsd settings
  server: ~                 # Statsd server name:port
  prefix: ~                 # Prefix to use for stats keys
pid-file: ~                 # Store the process id in this file when running in the background
stats-file: ~               # Periodically write statistics on peers and current traffic to the given file
hook: ~                     # Hook script to run for every event
hooks: {}                   # Multiple hook scripts to run for specific events
EOF

systemctl enable --now vpncloud@vpc0.service
systemctl restart vpncloud@vpc0.service
