# OVS integration

## Install

```shell
dnf install -y openvswitch
systemctl enable --now openvswitch

ovs-vsctl add-br ovsbr0

cat <<EOF > ovs0.xml
<network>
  <name>ovs0</name>
  <forward mode='bridge'/>
  <bridge name='ovsbr0'/>
  <virtualport type='openvswitch'/>
</network>
EOF
virsh net-define ovs0.xml
virsh net-start ovs0
virsh net-autostart ovs0

```


```shell
nmcli connection add type bridge ifname br0 con-name br0 ipv4.method manual ipv4.addresses "10.0.${SUBNET}.2/8"
nmcli con up br0
nmcli connection add type tun ifname vpc00 con-name vpc00 mode tap owner 0 ip4 0.0.0.0/24
nmcli con add type bridge-slave ifname vpc00 master br0
```


```shell
ovs-vsctl add-br ovsbr2
ovs-vsctl add-port ovsbr2 vxlan2 -- set interface vxlan2 type=vxlan options:remote_ip=YOUR_IP
ovs-vsctl show
cat <<EOF > test.xml
<network>
  <name>overlay-net-2</name>
  <forward mode='bridge'/>
  <bridge name='ovsbr2'/>
  <virtualport type='openvswitch'/>
  <portgroup name='overlay-pg'/>
</network>
EOF

virsh net-define test.xml
virsh net-start overlay-net-2

```

```shell
ID="0"
ovs-vsctl add-br ovs-br${ID}
ovs-vsctl add-port ovs-br${ID} enp37s0
cat <<EOF > test.xml
<network>
  <name>overlay${ID}</name>
  <forward mode="bridge"/>
  <bridge name="ovs-br${ID}"/>
</network>
EOF
sudo virsh net-define test.xml
sudo virsh net-start overlay${ID}
sudo virsh net-autostart overlay${ID}
```


```shell
dnf install vpncloud
dnf install && dnf upgrade
dnf update && dnf upgrade
dnf install rust -y
dnf install iproute
ip a
ping 8.8.8.8
dnf install ping
dnf install iputils
ip a
ping 172.17.0.2
dnf groupinstall -y 'Development Tools'
install -y ruby && gem install asciidoctor -v 2.0.10
dnf install -y ruby && gem install asciidoctor -v 2.0.10
dnf install cargo git
dnf install cargo git rust
git clone git://github.com/ddswd/vpncloud.git
git clone https://github.com/dswd/vpncloud.git
cd vpncloud/
cargo build --release
dnf install -y gcc-aarch64-linux-gnu
history
```


```shell
dnf groupinstall -y 'Development tools'
dnf install -y gcc git make install subversion gcc-c++ libressl
git clone https://github.com/peervpn/peervpn.git
cd peervpn/
make
```