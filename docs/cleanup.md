```shell
{
  for x in $(ls); do if [ "${x}" == "a-file-to-keep" ]; then
    echo keeping artifact "${x}"; else rm -rf "${x}";fi;
  done
  for x in $(vm.list | jq -r .[].name);do vm.rm $x;done
  rm -f ~/.ssh/known_hosts
}
```